$(function () {
    $('#contactoModal').on('show.bs.modal', function (e) {
        console.log('el modal se está mostrando');

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);

    });
    $('#contactoModal').on('shown.bs.modal', function (e) {
        console.log('el modal se mostró');
    });
    $('#contactoModal').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', false);
    });
    $('#contactoModal').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocultó');
    });
    $("[data-toggle='popover']").popover();
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval: 1000
    });
})